resource "aws_lambda_function" "hillel-java" {
  filename      = "files/lambda.zip"
  function_name = "hillel-java"
  role          = aws_iam_role.hillel-java-role.arn

  handler = "handler.Handler"
  runtime = "java17"

  environment {
    variables = {
      foo = "bar"
    }
  }
}

resource "aws_iam_role" "hillel-java-role" {
  name               = "hillel-java-role"
  assume_role_policy = data.aws_iam_policy_document.hillel-java-policy.json
}

data "aws_iam_policy_document" "hillel-java-policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }

}

resource "aws_lambda_function_url" "hillel-java-url" {
  function_name      = aws_lambda_function.hillel-java.function_name
  authorization_type = "NONE"
}

resource "aws_iam_policy" "hillel-java-execution-policy-execution-policy" {
  name   = "hillel-java-execution-policy-execution-policy"
  policy = data.aws_iam_policy_document.hillel-java-execution-policy-document.json
}


data "aws_iam_policy_document" "hillel-java-execution-policy-document" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "ssm:GetParameter"
    ]
    resources = [
      "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:parameter/*"
    ]
  }
}

resource "aws_iam_role_policy_attachment" "hillel-java-execution-policy-attachment" {
  role       = aws_iam_role.hillel-java-role.name
  policy_arn = aws_iam_policy.hillel-java-execution-policy-execution-policy.arn
}

