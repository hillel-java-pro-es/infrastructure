terraform {
  backend "s3" {
    bucket = "terraform-121831d9-83a3-4bc8-8b48-b32e3f9ab05e"
    region = "eu-central-1"
    key    = "infrastructure/hillel-terraform.tfstate"
  }
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {
}
